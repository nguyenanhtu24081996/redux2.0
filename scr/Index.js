import React, { Component } from 'react'
import {
    View,
    Text
} from 'react-native'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import Main from './component/Main'
export default class Index extends Component{
    render(){
        return(
            <Provider store={store}>
                <Main/>
            </Provider>
        )
    }
}

const defaultState = {
    arrWord:[
        {id:1, en: 'action', vn:'hành động', memorized:true, isShow:false},
        {id:2, en: 'actor', vn:'diễn viên', memorized:false, isShow:false},
        {id:3, en: 'activity', vn:'hoạt động', memorized:true, isShow:false},
        {id:4, en: 'bath', vn:'tắm', memorized:false, isShow:false},
        {id:5, en: 'bedroom', vn:'phòng ngủ', memorized:true, isShow:false},
        {id:6, en: 'yard', vn:'sân', memorized:false, isShow:false},
        {id:7, en: 'yesterday', vn:'hôm qua', memorized:true, isShow:false},
        {id:8, en: 'young', vn:'trẻ', memorized:true, isShow:false},
        {id:9, en: 'zero', vn:'số 0', memorized:false, isShow:false},
        {id:10, en: 'abandon', vn:'từ bỏ', memorized:true, isShow:false},
    ],
    filterStatus:'SHOW_ALL',
    isAdding:false,
}

const reducer = (state = defaultState, action) => {
    switch(action.type){
        case 'FILTER_SHOW_ALL':
            return { ...state, filterStatus:'SHOW_ALL'};
        case 'FILTER_MEMORIZED':
            return { ...state, filterStatus:'MEMORIZED'}
        case 'FILTER_NEED_PRACTICE':
            return { ...state, filterStatus:'NEED_PRACTICE'}
        case 'MEMORIZED': return {
                ...state, 
                arrWord:state.arrWord.map(e=>{
                    if(e.id !== action.id) return e;
                    return { ...e , memorized:!e.memorized}
                })
            };
        case 'SHOW': return { 
            ...state, 
            arrWord:state.arrWord.map(e=>{
                if(e.id !== action.id) return e;
                return { ...e , isShow:!e.isShow}
            })
        }
        case 'ADDING': 
            return{ ...state, isAdding:!state.isAdding}
        case 'ADD_WORD':
            return{
                ...state,
                arrWord:[{
                    id:state.arrWord.length+1,
                    en:action.en,
                    vn:action.vn,
                    memorized:false,
                    isShow:false
                }].concat(state.arrWord)
            }
        case 'DELETE_WORD':
           return{
                ...state,
                arrWord: state.arrWord.splice(action.id, 1),
            }
        default:
            break;
    };
    return state;
}

const store = createStore(reducer)

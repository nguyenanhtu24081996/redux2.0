import React, { Component } from 'react'
import {
    View,
    Text,
    Dimensions,
    FlatList,
    StyleSheet,
    TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'

class Filter extends Component{
    changeTextColor(statusName){
        const{myFilterStatus} = this.props;
        if(statusName === myFilterStatus) return { color:'yellow' }
        return styles.buttonText
    }

    setFilterStatus(actionType){
        this.props.dispatch({type:actionType})
    }
    render(){
        return(
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.setFilterStatus('FILTER_SHOW_ALL')}>
                    <Text style={this.changeTextColor('SHOW_ALL')}>SHOW_ALL</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setFilterStatus('FILTER_MEMORIZED')}>
                    <Text style={this.changeTextColor('MEMORIZED')}>MEMORIZED</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setFilterStatus('FILTER_NEED_PRACTICE')}>
                    <Text style={this.changeTextColor('NEED_PRACTICE')}>NEED_PRACTICE</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#583C3C',
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
       
    },
    buttonText:{
        margin:10,
        color:'white'
    }
})

function mapStateToProps(state) {
    return{
        myFilterStatus:state.filterStatus
    }
}

export default connect(mapStateToProps)(Filter)
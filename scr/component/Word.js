import React, { Component } from 'react'
import {
    View,
    Text,
    Dimensions,
    FlatList,
    StyleSheet,
    TouchableOpacity,
    ActionSheetIOS
} from 'react-native'
import { connect } from 'react-redux'

class Word extends Component{
    showMemorized(){
        this.props.dispatch({
            type:'MEMORIZED',
            id:this.props.myWord.id,
        })
    }
    showWord(){
        this.props.dispatch({
            type:'SHOW',
            id:this.props.myWord.id
        })
    }
    deleteWord(){
        this.props.dispatch({
            type:'DELETE_WORD',
            
        })
    }
    render(){
        const {en, vn, memorized, isShow} = this.props.myWord;
        const textDecorationLine = memorized ? 'line-through' : 'none' ;
        const memorizedText = memorized ? 'Forget' : 'Memorized';
        const show = isShow ? vn : '**********';
        return(
            <View style={styles.container}>
                <Text style={{textDecorationLine}}>{en}</Text>
                <Text>{show}</Text>
                <View style={styles.show}>
                    <TouchableOpacity onPress={this.showMemorized.bind(this)}>
                        <Text>{memorizedText}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.showWord()}>
                        <Text>Show</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.deleteWord()}>
                        <Text>Delete</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#D2DEF6',
        padding:10,
        margin:10
    },
    show:{
        flexDirection:'row',
        justifyContent:'space-around',
        marginTop:20,
    }
})

export default connect()(Word)

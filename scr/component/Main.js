import React, { Component } from 'react'
import {
    View,
    Text,
    Dimensions,
    FlatList,
    StyleSheet
} from 'react-native'
import { connect } from 'react-redux'
import Word from './Word'
import Filter from './Filter'
import Header from './Header'
import Form from './Form'
class Main extends Component{
    getWordList(){
        const {myArrWord,myFilter} = this.props;
        if(myFilter==='MEMORIZED') return myArrWord.filter(e=>e.memorized);
        if(myFilter === 'NEED_PRACTICE') return myArrWord.filter(e=>!e.memorized);
        return myArrWord;
    }
    render(){
        return(
           <View style={styles.container}>
               <Header/>
               {this.props.myAdding ? <Form/> : null}
               <View style={{flex:10}}>
                <FlatList
                    data={this.getWordList()}
                    renderItem={({ item }) => (
                        <Word myWord={item}/>
                    )}
                    keyExtractor={item => item.id}
                />
               </View>
               <Filter/>
           </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'yellow',
    }
})

function mapStateToProps(state) {
    return{
        myArrWord:state.arrWord,
        myFilter: state.filterStatus,
        myAdding: state.isAdding,
    }
}

export default connect(mapStateToProps)(Main);
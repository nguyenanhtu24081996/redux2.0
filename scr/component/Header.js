import React, { Component } from 'react'
import {
    View,
    Text,
    Dimensions,
    FlatList,
    StyleSheet,
    TextInput,
    TouchableOpacity
} from 'react-native'
import {connect} from 'react-redux'
class Header extends Component{
    render(){
        return(
            <View>
                <View style = {{alignItems:'center'}}>
                    <TouchableOpacity onPress={() => this.props.dispatch({type:'ADDING'})}>
                        <Text style={{fontSize:30}}>+</Text>
                    </TouchableOpacity>
                </View>
            </View>
           
        )
    }
}

export default connect()(Header)
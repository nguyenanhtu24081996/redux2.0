import React, { Component } from 'react'
import {
    View,
    Text,
    Dimensions,
    FlatList,
    StyleSheet,
    TextInput,
    TouchableOpacity
} from 'react-native'
import {connect} from 'react-redux'
class Form extends Component{
    constructor(props){
        super(props)
        this.state = {
            vn:'',
            en:'',
        };
     
    }
    onAdd(){
        const {vn, en} = this.state;
        this.props.dispatch({
            type:'ADD_WORD',
            en,
            vn,
        })
    }
    render(){
        return(
            <View>
                <TextInput 
                    style={{backgroundColor:'white', margin:10}}
                    value={this.state.en}
                    onChangeText={text => this.setState({en:text})}
                />
                <TextInput 
                    style={{backgroundColor:'white', margin:10}}
                    value={this.state.vn}
                    onChangeText={text => this.setState({vn:text})}
                />
                <View style = {{alignItems:'center'}}>
                    <TouchableOpacity onPress={this.onAdd.bind(this)}>
                        <Text>ADD</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default connect()(Form)